using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApagarPuerta : MonoBehaviour
{
    public GameObject UltimaPuerta;
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            GameObject otherObject = GameObject.Find("UltimaPuerta");
            Destroy(otherObject);
        }
    }
}