using System.Collections;
using System.Collections.Generic;
using UnityEngine.Video;
using UnityEngine;

public class VideoScript : MonoBehaviour
{
    [SerializeField]
    private VideoPlayer videoPlayerComponent;
    public GameObject VideoPlayerOjbect;

    private void Start()
    {
        if (videoPlayerComponent != null)
        {
            videoPlayerComponent.loopPointReached += OnVideoFinished;
        }
        else
        {
            Debug.LogError("VideoPlayer component is not assigned in the Inspector.");
        }
    }

    private void OnVideoFinished(VideoPlayer vp)
    {
        GameObject otherObject = GameObject.Find("VideoPlayerObject");
        Destroy(otherObject);
    }
}