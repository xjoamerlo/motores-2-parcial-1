using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstanciarLejos : MonoBehaviour
{
    public GameObject Rampa;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Rampa.SetActive(true);
        }
    }
}
